import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ConstantVariable } from '../../app/constant-variable';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../../providers/user/user';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture';
import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { VideoCapturePlus, VideoCapturePlusOptions, MediaFile } from '@ionic-native/video-capture-plus';

/**
 * Generated class for the DemandesDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;

const baseUrl = ConstantVariable.apiUrl+ConstantVariable.uploadVideo;
const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";
const URL = "http://wakodemo.tk:3131/media/uploadFile"
@IonicPage()
@Component({
  selector: 'page-demandes-detail',
  templateUrl: 'demandes-detail.html',
})
export class DemandesDetailPage {
  // @ViewChild(Content) content: Content;
	@ViewChild('myvideo') myVideo: any;

  fileToSend:string = '';

  selectedVideo: string; //= "https://res.cloudinary.com/demo/video/upload/w_640,h_640,c_pad/dog.mp4";
  uploadedVideo: string;

  isUploading: boolean = false;
  uploadPercent: number = 0;
  videoFileUpload: FileTransferObject;
  loader;
  token;
  requestId;

  data:any = {
    fanrequest_id:'',
    media:''
  };
  videoUri:any;
  request:any = {
    id:'',
    purchaserName:'',
    instructions:'',
  };

  VideosOk:any[];
  answered=false;

  datas:any = {
    booktalent_id: ''
  };
  pathForImage;

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera,
    private transfer: FileTransfer, private file: File,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController,
     public storage: Storage,private connectivityService: ConnectivityProvider,
    private userService: UserProvider,
    private utils: UtilsProvider,
    private mediaCapture: MediaCapture,private androidPermissions: AndroidPermissions/* ,private videoCapturePlus: VideoCapturePlus */) {

      this.checkRP();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DemandesDetailPage');
    
    this.request = this.navParams.get('data');
    this.data.fanrequest_id = this.request.id;
    this.datas.booktalent_id = this.request.id

   this.storage.get('token').then((token)=>{
     console.log(token);
     this.token = token;
     // this.ans();
     this.VideosOk = [];
     this.storage.get('VideosOk').then((videoIds)=>{
       // console.log(token);
       if (videoIds!=undefined) {
        this.VideosOk = videoIds;
       } else {
        this.VideosOk = [];
       }
       console.log('videoOK'+JSON.stringify(this.VideosOk));
       if(this.VideosOk){
         let element = this.VideosOk.find(ob => ob['id'] === this.request.id);
         console.log('data'+JSON.stringify(element));
            if (element) {
              this.answered=true;
            }
       }
       });
     });
     

  }

  ans(){
    // this.VideosOk = [];
      console.log('id is'+this.data.fanrequest_id);
      this.VideosOk.push({id:this.data.fanrequest_id})
    this.storage.set('VideosOk', this.VideosOk);
    console.log(JSON.stringify(this.VideosOk));
    this.navCtrl.pop();
    }

  takeVideo() {

		let options: CaptureVideoOptions = {
			limit: 1,
			quality: 0,
			duration: 60
	    }

		this.mediaCapture.captureVideo(options).then((res: MediaFile[]) => {
			let capturedFile = res[0];
      let fileName = capturedFile.name;
      console.log("capturedFile "+JSON.stringify(capturedFile));
      let type = capturedFile.type;
      console.log("type "+type);
      capturedFile.getFormatData((data)=> console.log("getFormatData "+ JSON.stringify(data)))
      let dir = capturedFile['localURL'].split('/');
      console.log("dir "+dir);
			dir.pop();
      console.log("dir.pop "+dir);

			let fromDirectory = dir.join('/');
      console.log("fromDirectory "+fromDirectory);
      // this.selectedVideo = capturedFile['fullPath'];
			this.copyFileToLocalDir(fromDirectory, fileName, this.createFileName());
        },
    	(err: CaptureError) => console.error(err));
  }
  
  // takeVideo() {

	// 	let options: VideoCapturePlusOptions = {
	// 		limit: 1,
  //     highquality: true,
  //     portraitOverlay: 'assets/imgs/Cameo-Logo.png',
	// 		duration: 60
	//     }

	// 	this.videoCapturePlus.captureVideo(options).then((res: MediaFile[]) => {
	// 		let capturedFile = res[0];
	// 		let fileName = capturedFile.name;
  //     let type = capturedFile.type;
  //     console.log("type "+type);
  //     console.log("capturedFile "+JSON.stringify(capturedFile));
  //     let dir = capturedFile['fullPath'].split('/');
  //     console.log("dir "+dir);
	// 		dir.pop();
  //     console.log("dir.pop "+dir);

	// 		let fromDirectory = dir.join('/');
  //     console.log("fromDirectory "+fromDirectory);
  //     capturedFile.getFormatData((data)=> console.log("getFormatData "+ JSON.stringify(data)));
	// 		this.copyFileToLocalDir(fromDirectory, fileName, this.createFileName());
  //       },
  //   	(err) => console.error(err));
  // }
  // Create a new name for the image
	private createFileName() {
		var d = new Date(),
		n = d.getTime(),
		newFileName =  n + ".mp4";
		this.fileToSend = newFileName;
		return newFileName;
  }
  
  // Copy the image to a local folder
	private copyFileToLocalDir(namePath, currentName, newFileName) {
		this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      let path = cordova.file.dataDirectory + newFileName;
      console.log("path or  pathForImage "+path);
          let url = path.replace(/^file:\/\//, '');
          console.log("url "+url);
          // let video = this.myVideo.nativeElement;
          // video.src = url;
          this.pathForImage = cordova.file.dataDirectory + newFileName;
          this.selectedVideo = url;
          this.file.checkFile(cordova.file.dataDirectory,newFileName).then(success => {
            console.log("copied file exist ");
          },error => {
            console.log('copied file not exist');
          });
      // 		this.file.readAsDataURL(cordova.file.dataDirectory, newFileName).then(imageBase64 => {

      // 			let video = this.myVideo.nativeElement;
      //   video.src = url;
      //   // this.data.media=imageBase64;

      //   this.selectedVideo = imageBase64;
			// });

		}, error => {
			this.utils.makeToast('Erreur lors de l\'enregistrement.').present();
		});
	}


  sendVideo(){	

	  if (this.fileToSend == "") {
            this.utils.makeToast('Veuillez faire une vidéo').present();
            return;
		}

		if (this.connectivityService.isOnline()) {
			let loader = this.utils.makeLoader('Envoi de votre vidéo en cours...');
			loader.present();

			// let datas = {
			// 	code: 'ca'
			// }
      // let datas:any = {
      //   booktalent_id: this.request.id,
      // };
			// this.userService.storeVideo(this.fileToSend,this.datas,this.token).then((response)=>{
        this.userService.storeVideo(this.pathForImage,this.fileToSend,this.datas,this.token).then((response)=>{
				// this.events.publish('getNewLight');
				
				// this.data = {
				// 	title:'',
				// 	content:''
				// }
				// this.fileToSend = "";
        // this.myVideo.nativeElement.src = '';
        loader.dismiss();
        console.log('upload response '+JSON.stringify(response));
        this.utils.makeToast('Votre vidéo a été publié.').present();
        this.ans();

      //   let strippedName = response.name.replace(/"/g, '');
      //   console.log('name '+strippedName);
      //  let data2:any = {
      //     booktalent_id: this.request.id,
      //     media: strippedName
      //   };

        // this.postVideo(data2);


			}).catch((error)=>{
        // this.utils.makeAlert('Une erreur est survenue','veuillez réessayer.').present();
        this.utils.makeAlert('Vidéo non publiée','Une vidéo a déja été publié pour cette demande. Vous ne pouvez pas envoyer de nouvelles vidéos pour le moment').present();
        loader.dismiss();
        // this.ans();
			});
		}else{ 
      this.utils.makeToast('Vérifier votre connexion internet').present();
    }
  } 
  

  postVideo(data){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        this.userService.postVideo(data).then(response =>{
            loader.dismiss();
            this.utils.makeToast('Votre video a bien été publié.').present();
            console.log(response);

            // this.VideosOk.push(this.request.id)
            // this.storage.set('VideosOk', this.VideosOk);
            this.ans();
            // this.navCtrl.pop();

        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur lors du chargement').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }


  showLoader() {
    this.loader = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loader.present();
  }

  dismissLoader() {
    this.loader.dismiss();
  }

  presentAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  cancelSelection() {
    this.selectedVideo = null;
    this.uploadedVideo = null;
  }

  selectVideo() {
    const options: CameraOptions = {
      mediaType: this.camera.MediaType.VIDEO,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      // destinationType: this.camera.DestinationType.DATA_URL,
    }

    this.camera.getPicture(options)
      .then( async (videoUrl) => {
        if (videoUrl) {
          this.showLoader();
          this.uploadedVideo = null;
          
          var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
          var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);

          // var video = cordova.file.dataDirectory + videoUrl;
          // this.data.media = video;

          // this.videoUri=video;

          dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
          
          try {
            var dirUrl = await this.file.resolveDirectoryUrl(dirpath);
            var retrievedFile = await this.file.getFile(dirUrl, filename, {});

          } catch(err) {
            this.dismissLoader();
            return this.presentAlert("Error","Something went wrong.");
          }
          
          retrievedFile.file( data => {
              this.dismissLoader();
              if (data.size > MAX_FILE_SIZE) return this.presentAlert("Error", "You cannot upload more than 5mb.");
              if (data.type !== ALLOWED_MIME_TYPE) return this.presentAlert("Error", "Incorrect file type.");

              this.selectedVideo = retrievedFile.nativeURL;
          });
        }
      },
      (err) => {
        console.log(err);
      });
  }

  uploadVideo() {
    var url = encodeURI(baseUrl);
    
    var filename = this.selectedVideo.substr(this.selectedVideo.lastIndexOf('/') + 1);
      
    var options: FileUploadOptions = {
      fileName: filename,
      fileKey: 'media',
      mimeType: "video/mp4",
      httpMethod: 'POST',
      chunkedMode: false,
      // params: {code: "ca"},
      params: {fanrequest_id:this.data.fanrequest_id},
      headers: {'Content-Type': "application/json", 'Accept': "application/json", 'Authorization': "Bearer " + this.token}
    }

    this.videoFileUpload = this.transfer.create();

    this.isUploading = true;

    this.videoFileUpload.upload(this.selectedVideo, url, options)
      .then((data)=>{
        this.isUploading = false;
        this.uploadPercent = 0;
        return JSON.parse(data.response);
      })
      .then((data) => {        
        this.uploadedVideo = data.url;
        this.presentAlert("Success", "Video upload was successful.");
        console.log(data);
      })
      .catch((err)=>{
        this.isUploading = false;
        this.uploadPercent = 0;
        console.log(err);
        this.presentAlert("Error", "Error uploading video.");
      });

    this.videoFileUpload.onProgress((data) => {
      this.uploadPercent = Math.round((data.loaded/data.total) * 100);
    });

  }

  cancelUpload() {
    this.videoFileUpload.abort();
    this.uploadPercent = 0;
  }

  checkRP(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => {
        if (result.hasPermission) {
          // code
          this.checkWP();
        } else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => {
            if (result.hasPermission) {
              // code
              this.checkWP();
            }
          });
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );
  }

  checkWP(){
    
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.	WRITE_EXTERNAL_STORAGE).then(
      result => {
        if (result.hasPermission) {
          // code
        } else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.	WRITE_EXTERNAL_STORAGE).then(result => {
            if (result.hasPermission) {
              // code
            }
          });
        }
        
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.	WRITE_EXTERNAL_STORAGE)
    );

  }

}

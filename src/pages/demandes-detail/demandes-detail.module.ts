import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemandesDetailPage } from './demandes-detail';

@NgModule({
  declarations: [
    DemandesDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DemandesDetailPage),
  ],
})
export class DemandesDetailPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoReçuesPage } from './video-reçues';

@NgModule({
  declarations: [
    VideoReçuesPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoReçuesPage),
  ],
})
export class VideoReçuesPageModule {}

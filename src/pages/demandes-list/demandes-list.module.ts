import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemandesListPage } from './demandes-list';

@NgModule({
  declarations: [
    DemandesListPage,
  ],
  imports: [
    IonicPageModule.forChild(DemandesListPage),
  ],
})
export class DemandesListPageModule {}

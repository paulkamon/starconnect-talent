import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { UtilsProvider } from '../../providers/utils/utils';
import { UserProvider } from "../../providers/user/user";
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

/**
 * Generated class for the EditProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profil',
  templateUrl: 'edit-profil.html',
})
export class EditProfilPage {
  picture:any;

  photoUrl:string;

  data = {
    talent_id :'',
  };
  token;

  data1={
    talent_id :'',
  };

  imageURI:any;
imageFileName:any;
  password: any;
  oldpassword:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public camera: Camera,
    public userPrvd: UserProvider,
    private utilsPrvd: UtilsProvider, private connectivityService: ConnectivityProvider,
    private userService: UserProvider,public loadingCtrl: LoadingController,public toastCtrl: ToastController,
    private transfer: FileTransfer,
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilPage');
    this.storage.get('user').then((user)=>{
      console.log(user);
      // this.data.fullname = user.fullname;
      // this.data.username = user.username;
      // this.data.email = user.email;
      // this.data.slogan = user.slogan;
      this.data.talent_id = user.id;
      this.data1.talent_id = user.id;
      this.storage.get('picture').then((picture)=>{
        if (picture) {
          this.photoUrl = picture;
        } else {
          if(user.photo){   
            this.photoUrl = ConstantVariable.mediaUrl+user.photo;
          }else{
            this.photoUrl = 'assets/imgs/profile.png';
          }
        }   
        this.storage.get('token').then((token)=>{
          console.log(token);
          this.token=token;
          this.storage.get('password').then((password)=>{
            console.log(password);
            this.password=password;
            })
          }) 
    }); 
    })
  }

  actionSheet(){
    this.utilsPrvd.makeActionSheet('Prendre une photo',[{
      icon: 'aperture',
      text: 'Galerie',
      handler: () => {
        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
      }
    },{
      icon: 'camera',
      text: 'Camera',
      handler: () => {
        this.takePicture(this.camera.PictureSourceType.CAMERA);
      }
    }]).present();
  }

  takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options : CameraOptions = {
      quality: 50,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      this.picture = 'data:image/jpeg;base64,' + imagePath;
      this.imageURI = imagePath;
      if(this.imageURI){   
        this.photoUrl = this.imageURI;
      }
      // this.data1.media = this.picture;
    });
  }

  edit(){
    this.storage.set('picture', this.photoUrl).then((data) =>{
      // this.navCtrl.pop();
      // this.viewCtrl.dismiss({ 'foo': 'bar' });
      this.uploadFile();
    })
  }

  updatepp() {
    if(this.connectivityService.isOnline()){
      if (this.oldpassword && this.oldpassword != this.password) {
        this.utilsPrvd.makeToast('mauvais mot de passe').present();
        return;
      } 
      let loader = this.utilsPrvd.makeLoader('Modification en cours...');
      loader.present().then(()=>{
        this.userService.updatepp(this.data , this.token).then(response =>{
            loader.dismiss();
            console.log(response);
            this.storage.set('user', response.talent).then((data) =>{
              this.utilsPrvd.makeToast('Enregistrement Réussi').present();
              this.navCtrl.pop();
            })
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utilsPrvd.makeToast('erreur lors du chargement').present();
        })
      })
    }
    else{
      this.utilsPrvd.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
  
    let options: FileUploadOptions = {
      fileKey: 'media',
      fileName: 'media',
      chunkedMode: false,
      mimeType: "image/jpeg",
      params: this.data1,
      headers: {'Accept': "application/json", 'Authorization':'Bearer '+this.token}
    }
  
    fileTransfer.upload(this.photoUrl, ConstantVariable.apiUrl+ConstantVariable.updatepp, options)
      .then((data) => {
      console.log(data+" Uploaded Successfully");
      // this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
      loader.dismiss();
      this.presentToast("Envoi réussi");
      this.navCtrl.pop();
    }, (err) => {
      console.log(err);
      loader.dismiss();
      this.presentToast("erreur");
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';

/**
 * Generated class for the StarDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-star-detail',
  templateUrl: 'star-detail.html',
})
export class StarDetailPage {
  star:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    this.star=this.navParams.get('star');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StarDetailPage');
  }

  back(){
    this.navCtrl.pop();
  }

  demand(){
    this.navCtrl.push('FaireDemandePage',{star: this.star});
    // this.presentPrompt();
  }

  // presentPrompt() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Login',
  //     message: 'le motif de la demande',
  //     inputs: [
  //       {
  //         name: 'username',
  //         placeholder: 'Username'
  //       },
  //       {
  //         name: 'password',
  //         placeholder: 'Password',
  //         type: 'password'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: data => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'confirmer',
  //         handler: data => {
  //           // if (User.isValid(data.username, data.password)) {
  //           //   // logged in!
  //           // } else {
  //           //   // invalid login
  //           //   return false;
  //           // }
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  // }
}

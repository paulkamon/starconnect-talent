import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StarDetailPage } from './star-detail';

@NgModule({
  declarations: [
    StarDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(StarDetailPage),
  ],
})
export class StarDetailPageModule {}

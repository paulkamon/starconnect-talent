import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  data = {
    email: '',
    password: '',
    provider: 'talents',
  };


  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage,
    private alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


login() {
  if(this.connectivityService.isOnline()){
    let loader = this.utils.makeLoader('Connexion en cours...');
    loader.present().then(()=>{
      console.log(JSON.stringify(this.data));
      this.authService.login(this.data).then(response =>{
          loader.dismiss();
          // this.response = response;
          console.log(response);
          this.storage.set('token', response.access_token);
          this.storage.set('user', response.user);
          this.storage.set('token_type', response.token_type);
          this.storage.set('password', this.data.password);
          this.navCtrl.setRoot('TalentDashboardPage',{});
          //  if(this.response.user.typeuser_id == '0'){
          //     this.navCtrl.setRoot('HomePage');
          //     this.storage.set('isLoggedIn', 'employeur');
          //   }
          //   else{
              // this.navCtrl.setRoot('HomeemployePage');
          //     this.storage.set('isLoggedIn', 'employe');

          //   }
      }).catch((error)=>{
        console.log(error);

        loader.dismiss();
        this.utils.makeToast('Votre numéro ou votre mot passe est incorrecte!').present();
      })
    })
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}

signup(){
  this.navCtrl.push('RegisterPage');
}

presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Reset password',
      message: 'Are you sure ?',
      inputs: [
        {
          name: 'email',
          placeholder: 'Enter you email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Reset',
          handler: data => {
            if (data.email) {
             this.resetPwd(data.email)
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  resetPwd(email) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('Connexion en cours...');
      loader.present().then(()=>{
        let datas = {
          email:email,
          provider: 'talents',
          lang: 'fr'
        }
        this.authService.resetPwd(datas).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response);
            this.showAlert();
        
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('something went wrong').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Mot de passe réinitialisé avec succès',
      subTitle: 'un mail contenant votre nouveau mot de passe a été envoyé',
      buttons: ['OK']
    });
    alert.present();
  }
}

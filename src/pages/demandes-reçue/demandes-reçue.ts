import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';

/**
 * Generated class for the DemandesReçuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-demandes-recue',
  templateUrl: 'demandes-reçue.html',
})
export class DemandesReçuePage {
  demandesList:any[] = [];
  isloading: boolean = false;
  listExist: boolean = true;
  list: any[];
  VideosOk:any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private userService: UserProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
      this.storage.get('token').then((token)=>{
        console.log(token);
        this.listDemandes(token);
        })  
      }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DemandesReçuePage');
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter DemandesReçuePage');
    this.storage.get('VideosOk').then((videoIds)=>{
      // console.log(token);
      if (videoIds!=undefined) {
       this.VideosOk = videoIds;
      } else {
       this.VideosOk = [];
      }
      });
  }

  push(i){
    console.log(this.demandesList[i]);
    this.navCtrl.push('DemandesDetailPage',{data: this.demandesList[i]})
  }

  public checkin(i){
    console.log(this.demandesList[i]);
    // if(this.VideosOk){
      let element = this.VideosOk.find(ob => ob['id'] === this.demandesList[i].id);
      console.log('data'+JSON.stringify(element));
         if (element!=undefined) {
           return true;
         } else {
          return false;
         }
    // }
  }

  public convertTodate(string){
    return new Date(string.replace(/-/g, '/'));
  }
  
  listDemandes(token) {
    if(this.connectivityService.isOnline()){
      this.isloading=true;
      // let loader = this.utils.makeLoader('chargement...');
      // loader.present().then(()=>{
        this.userService.listDemandesReçues(token).then(response =>{
            // loader.dismiss();
            this.isloading=false;
            
            this.list = response.requests
            this.demandesList = this.list.reverse();
            if (this.demandesList.length > 0) {
              this.listExist=true;
            }
            else{
              this.listExist=false;
            }
            console.log(response);
            
        }).catch((error)=>{
          console.log(error);
          this.isloading=false;
          // loader.dismiss();
          this.utils.makeToast('erreur lors du chargement').present();
        })
      // })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}

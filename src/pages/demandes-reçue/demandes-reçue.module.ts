import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemandesReçuePage } from './demandes-reçue';

@NgModule({
  declarations: [
    DemandesReçuePage,
  ],
  imports: [
    IonicPageModule.forChild(DemandesReçuePage),
  ],
})
export class DemandesReçuePageModule {}

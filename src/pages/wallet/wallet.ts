import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
  transactionList:any[];
  wallet;
  amount=0;
  dailyList:any[];
  isloading: boolean = false;
  listExist: boolean = true;
  transaction;
  dailyListExist: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private userService: UserProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
      this.transaction="today"
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');
    this.storage.get('token').then((token)=>{
      console.log(token);
      this.userInfo(token);
      })  
  }

  back(){
    this.navCtrl.pop();
  }

  userInfo(token) {
    if(this.connectivityService.isOnline()){
      this.isloading=true;
      // let loader = this.utils.makeLoader('Loading...');
      // loader.present().then(()=>{
        this.userService.listDemandesReçues(token).then(response =>{
            // loader.dismiss();
            this.isloading=false;
            this.transactionList = response.operations;
            this.wallet=response.wallet;
            // this.amount=response.wallet.amount;

            if (this.transactionList != null) {
            
            this.dailyList = [];

            this.transactionList.forEach(element => {

              var userEntered = new Date(element.created_at.replace(/-/g, '/')); 
              userEntered.setHours(0,0,0,0);
              var now = new Date();
              var today = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() ));
              console.log("userEntered "+userEntered.getTime());
              console.log("now "+now.getTime());
              console.log("today "+ today.getTime());
              console.log("userEntered "+userEntered);
              console.log("now "+now);
              console.log("today "+ today);
              if(userEntered.getTime() < today.getTime()){
                // alert("date is past");
              }
              else if(userEntered.getTime() == today.getTime()){
                // alert("date is today");
                this.dailyList.push(element)
              }
              // else
              //   alert("date is future");
              // if (element.created_at) {
                // this.dailyList.push(element.video.id*8782487826728)
              // }
              // console.log('videolist'+this.videoList);
            });
          }
          if (this.dailyList.length > 0) {
            this.dailyListExist=true;
          } else{
            this.dailyListExist=false;
          }

            if (this.transactionList.length > 0) {
              this.listExist=true;
              console.log("Tlist");
            }
            else{
              this.listExist=false;
              console.log("noTlist");
            }
            console.log(response);

            if (this.transaction=="today") {
              if (this.dailyList != null) {
              for (let t of this.dailyList) {
                this.amount = this.amount + t.amount;
               }
              }
            } 
            
        }).catch((error)=>{
          console.log(error);
          this.isloading=false;
          // loader.dismiss();
          // this.utils.makeToast('erreur lors du chargement').present();
        })
      // })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  segmentChanged(event){
    if (this.transaction=="today") {
      this.amount=0;
      if (this.dailyList != null) {
      for (let t of this.dailyList) {
        this.amount = this.amount + t.amount;
       }
      }
    } else {
      this.amount=0;
      if (this.transactionList) {
      for (let t of this.transactionList) {
        this.amount = this.amount + t.amount;
       }
      }
    }
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilsProvider } from '../../providers/utils/utils';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  data = {
    firstname: '',
    name: '',    
    pseudo: '',
    email: '',
    password:'',
    confirm_password:'',
    country:'CI',
    isAcceptGCU: 1,
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private utils: UtilsProvider,
    private authService: AuthServiceProvider,
    public storage: Storage,
    private connectivityService: ConnectivityProvider,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  register(){
   
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('Connexion en cours...');

      if (this.data.name && this.data.firstname && this.data.pseudo && this.data.email && this.data.country) {   
      loader.present().then(()=>{
        if (this.data.password == this.data.confirm_password) {
          this.authService.register(this.data).then(response =>{
              loader.dismiss();
              console.log(response);
              this.storage.set('token', response.access_token);
              this.storage.set('user', response.user);
              this.storage.set('token_type', response.token_type);
              this.storage.set('password', this.data.password);
              this.navCtrl.setRoot(TabsPage,{});
          }).catch((error)=>{
            console.log(error);
  
            loader.dismiss();
            this.utils.makeToast('Problème constaté, veuillez reprendre votre inscription!').present();
          })
        }
        else{
          loader.dismiss();
          let toast = this.utils.makeToast('Votre mot de passe n\'est pas conforme à celui de la confirmation');
          toast.present();
        }
        
      })
    }else{
      let toast = this.utils.makeToast('Veuillez renseigner tous les champs requis');
      toast.present();
    }
  }
  }

}

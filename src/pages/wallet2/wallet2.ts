import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the Wallet2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet2',
  templateUrl: 'wallet2.html',
})
export class Wallet2Page {
  isloading: boolean = false;
  wallet;
  date: Date;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private userService: UserProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');
    this.storage.get('token').then((token)=>{
      console.log(token);
      this.userInfo(token);
      })  
  }

  public convertTodate(string){
    return new Date(string.replace(/-/g, '/'));
  }

  back(){
    this.navCtrl.pop();
  }

  userInfo(token) {
    if(this.connectivityService.isOnline()){
      this.isloading=true;
      // let loader = this.utils.makeLoader('Loading...');
      // loader.present().then(()=>{
        this.userService.walletDetails(token).then(response =>{
            // loader.dismiss();
            this.wallet=response;
            this.isloading=false;
            var dat = response.dates.start.substring(0, 10);
            console.log('dat',dat);
            this.date =new Date(dat.replace(/-/g, '/'));
            // this.amount=response.wallet.amount;
            console.log(response);
            
        }).catch((error)=>{
          console.log(error);
          this.isloading=false;
          // loader.dismiss();
          // this.utils.makeToast('erreur lors du chargement').present();
        })
      // })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }


}

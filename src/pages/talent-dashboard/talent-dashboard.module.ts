import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TalentDashboardPage } from './talent-dashboard';
// import { TranslateModule , TranslateLoader} from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
// import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http } from '@angular/http';

// export function createTranslateLoader(http: Http) {
//   return new TranslateHttpLoader(http, './assets/i18n/', '.json');
//  }
 
@NgModule({
  declarations: [
    TalentDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(TalentDashboardPage),
  //   TranslateModule.forChild(/* {
  //     loader: {provide: TranslateLoader, useFactory: (createTranslateLoader), deps: [Http]},
  //     isolate: true
  // } */)
  ],
  // exports: [
  //   TalentDashboardPage
  // ]
})
export class TalentDashboardPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';
import { ContactPage } from '../contact/contact';
import { LoginPage } from '../login/login';

/**
 * Generated class for the TalentDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-talent-dashboard',
  templateUrl: 'talent-dashboard.html',
})
export class TalentDashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private connectivityService: ConnectivityProvider,
    private utils: UtilsProvider,
    private storage: Storage,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TalentDashboardPage');
    this.storage.get('user').then((user)=>{
      if (user) {
        console.log(user);
        let id = user.id.toString();
        console.log('id ',id);
        window['plugins'].OneSignal.setExternalUserId(id);
        
      }
    });
  }

  demandes(){
    this.navCtrl.push('DemandesReçuePage');
  }

  socialCharge(){
    this.navCtrl.push('SocialchargePage');
  }

  stopwork(){
    this.utils.makeToast('Cette fonctionnalité n\'a pas encore implémentée').present();
    //this.navCtrl.push('StopworkPage');
  }

  wallet(){
    this.navCtrl.push('Wallet2Page');
  }

  userDetail(){
    // this.utils.makeToast('Cette fonctionnalité n\'a pas encore implémentée').present();
    this.navCtrl.push(ContactPage,{});
    }

    disconnect(){   
      this.storage.clear().then(()=>{
        this.navCtrl.setRoot(LoginPage);
        window['plugins'].OneSignal.removeExternalUserId();
        console.log('cleared');
      })
      .catch(()=>{
        this.utils.makeToast('Veuillez réessayer svp!').present();
      });
    
    }

    showConfirm() {
      const confirm = this.alertCtrl.create({
        // title: 'Use this lightsaber?',
        message: 'voulez vous vraiment vous déconnecter?',
        buttons: [
          {
            text: 'annuler',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'confirmer',
            handler: () => {
              console.log('Agree clicked');
              this.disconnect();
            }
          }
        ]
      });
      confirm.present();
    }
}

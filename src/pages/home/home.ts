import { Component } from '@angular/core';
import { MbscScrollViewOptions } from '@mobiscroll/angular';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tmp:any = [];

  scrollViewOptions: MbscScrollViewOptions = {
    layout: 'fixed',
    itemWidth: 134,
    snap: false
};

groupes = [{
  name: 'Music Stars',
  list: [{
    image: 'http://www.linfodrome.com/media/k2/items/cache/54a91e11fe1884a97da746055aa13126_L.jpg',
    title: 'Dj Arafat',
    dev: 'Yorogang prod',
    rank: 4.4
  }, {
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJu13Ip1voC8jGIYHd-Q6F0n-Jkt_XOFWTr-niIsXl9onqXIlVWA',
    title: 'Josey',
    dev: 'josey',
    rank: 4.7
  }, {
    image: 'https://konect.africa.com/wp-content/uploads/2018/11/Ariel-sheney.jpg',
    title: 'Ariel Sheney',
    dev: 'Ariel Sheney',
    rank: 4.3
  }, {
    image: 'https://img.mobiscroll.com/demos/5k.png',
    title: '5K Run: 5K Runner®',
    dev: 'Fitness22',
    rank: 4.4
  }, {
    image: 'https://img.mobiscroll.com/demos/nuzzelnws.png',
    title: 'Nuzzel News',
    dev: 'Nuzzel, Inc.',
    rank: 4.3
  }, {
    image: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT6uwPPBnHfAAUcSzxr3iq9ou1CZ4f_Zc2O76i5A4IyoymIVwjOMXwUFTGSrVGcdGT9vQY',
    title: 'The Rolling Stones',
    dev: 'The Rolling Stones',
    rank: 4.5
  }, {
    image: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT-FbU5dD_Wz472srRIvoZAhyGTEytx9HWGusbhYgSc2h0N6AqqRrDwzApmyxZoIlyxDcU',
    title: 'Pink Floyd',
    dev: 'Pink Floyd Labs',
    rank: 4.5
  }, {
    image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTGpH07f9zeucoOs_stZyIFtBncU-Z8TDYmJgoFnlnxYmXjJEaitmxZNDkNvYnCzwWTySM',
    title: 'The Beatles',
    dev: 'The Beatles',
    rank: 4.1
}]
},
{
name: 'Footballeurs',
list: [{
  image: 'http://www.rumeursdabidjan.net/media/k2/items/cache/a1d1dc8977cdc0e151683e442c926a10_XL.jpg',
  title: 'Didier Drogba',
  dev: 'Footbal ',
  rank: 4.3
}, {
  image: 'https://img.mobiscroll.com/demos/realplayer.png',
  title: 'RealPlayer®',
  dev: 'RealNetworks, Inc.',
  rank: 4.3
}, {
  image: 'https://img.mobiscroll.com/demos/motogal.png',
  title: 'Motorola Gallery',
  dev: 'Motorola Mobility LLC. ',
  rank: 3.9
}, {
  image: 'http://www.linfodrome.com/media/k2/items/cache/54a91e11fe1884a97da746055aa13126_L.jpg',
  title: 'Reverse Movie FX',
  dev: 'Bizo Mobile',
  rank: 4.6
}, {
  image: 'https://img.mobiscroll.com/demos/sure.png',
  title: 'SURE Universal Remote',
  dev: 'Tekoia Ltd.',
  rank: 4.4
}, {
  image: 'https://img.mobiscroll.com/demos/ringdriod.png',
  title: 'Ringdroid',
  dev: 'Ringdroid Team ',
  rank: 4.4
}, {
  image: 'https://img.mobiscroll.com/demos/funny.png',
  title: 'Funny Camera - Video Booth Fun',
  dev: 'Kiloo',
  rank: 4.1
}, {
  image: 'https://img.mobiscroll.com/demos/gif.png',
  title: 'GIF Keyboard',
  dev: 'IRiffsy, Inc.',
  rank: 4.1
}, {
  image: 'https://img.mobiscroll.com/demos/bs.png',
  title: 'BSPlayer',
  dev: 'BSPlayer media',
  rank: 4.4
}, {
  image: 'https://img.mobiscroll.com/demos/vac.png',
  title: 'video audio cutter',
  dev: 'mytechnosound ',
  rank: 4.3
}]
},
{
name: 'Rock Stars',
list: [{
  image: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSulfJcjBhxxW2NBBn9KbE3B4BSeh0R7mQ38wUi_zpJlQrMoDWh_qFcMelE_tjtAERUPTc',
  title: 'The Ramones',
  dev: 'The Ramones, Inc. ',
  rank: 4.4
}, {
  image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRA3jz0uhVypONAKWUve80Q6HASvuvZiohl4Sru5ZihkAsjWiaGjocfxd0aC3H7EeFk5-I',
  title: 'The Jimi Hendrix Experience',
  dev: 'The Jimi Hendrix Experience',
  rank: 4.7
}, {
  image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRIslVN9cJJ6YuV0y7JihAyA63JDhXGhkCVxHIRE-IoaF-rpefjIXO5osA24QvN9iCptC8',
  title: 'Van Halen',
  dev: 'Van Halen',
  rank: 4.3
}, {
  image: 'https://img.mobiscroll.com/demos/5k.png',
  title: '5K Run: 5K Runner®',
  dev: 'Fitness22',
  rank: 4.4
}, {
  image: 'https://img.mobiscroll.com/demos/nuzzelnws.png',
  title: 'Nuzzel News',
  dev: 'Nuzzel, Inc.',
  rank: 4.3
}, {
  image: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT6uwPPBnHfAAUcSzxr3iq9ou1CZ4f_Zc2O76i5A4IyoymIVwjOMXwUFTGSrVGcdGT9vQY',
  title: 'The Rolling Stones',
  dev: 'The Rolling Stones',
  rank: 4.5
}, {
  image: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT-FbU5dD_Wz472srRIvoZAhyGTEytx9HWGusbhYgSc2h0N6AqqRrDwzApmyxZoIlyxDcU',
  title: 'Pink Floyd',
  dev: 'Pink Floyd Labs',
  rank: 4.5
}, {
  image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTGpH07f9zeucoOs_stZyIFtBncU-Z8TDYmJgoFnlnxYmXjJEaitmxZNDkNvYnCzwWTySM',
  title: 'The Beatles',
  dev: 'The Beatles',
  rank: 4.1
}]
}];

data = {};
listArtiste=[];

  constructor(public navCtrl: NavController,  private connectivityService: ConnectivityProvider,
    private userService: UserProvider,
    private utils: UtilsProvider,
    public storage: Storage) {

    // this.tmp = [
    //   {desc: 'The Ramones', image:'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSulfJcjBhxxW2NBBn9KbE3B4BSeh0R7mQ38wUi_zpJlQrMoDWh_qFcMelE_tjtAERUPTc'},
    //   {desc: 'The Beatles', image:'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTGpH07f9zeucoOs_stZyIFtBncU-Z8TDYmJgoFnlnxYmXjJEaitmxZNDkNvYnCzwWTySM'},
    //   {desc: 'Pink Floyd', image:'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT-FbU5dD_Wz472srRIvoZAhyGTEytx9HWGusbhYgSc2h0N6AqqRrDwzApmyxZoIlyxDcU'},
    //   {desc: 'The Rolling Stones', image:'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT6uwPPBnHfAAUcSzxr3iq9ou1CZ4f_Zc2O76i5A4IyoymIVwjOMXwUFTGSrVGcdGT9vQY'},
    //   {desc: 'The Jimi Hendrix Experience', image:'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRA3jz0uhVypONAKWUve80Q6HASvuvZiohl4Sru5ZihkAsjWiaGjocfxd0aC3H7EeFk5-I'},
    //   {desc: 'Van Halen', image:'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRIslVN9cJJ6YuV0y7JihAyA63JDhXGhkCVxHIRE-IoaF-rpefjIXO5osA24QvN9iCptC8'}
    // ];
  
  }

  ionViewDidLoad() {
    this.listTalents();
  }


  pushemp(i){
    console.log(this.listArtiste[i]);
    this.navCtrl.push('StarDetailPage',{star: this.listArtiste[i]})
    }

  // showall(i){
  //   console.log(this.groupes[i].list.length);
  //   this.navCtrl.push('ShowallPage',{clist: this.groupes[i].list})
  // }

  listTalents() {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('Connexion en cours...');
      loader.present().then(()=>{
        this.userService.listTalents(this.data).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response.talents);
            this.listArtiste=response.talents;
            
            
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur lors du chargement').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}

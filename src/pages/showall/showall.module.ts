import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowallPage } from './showall';

@NgModule({
  declarations: [
    ShowallPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowallPage),
  ],
})
export class ShowallPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ShowallPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-showall',
  templateUrl: 'showall.html',
})
export class ShowallPage {

  clist:any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowallPage');
    this.clist = this.navParams.get('clist');
  }

  push(j){
    console.log(this.clist[j].title);
    this.navCtrl.push('StarDetailPage',{star: this.clist[j]})
  }
  back(){
    this.navCtrl.pop();
  }
}

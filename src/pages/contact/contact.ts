import { Component } from '@angular/core';
import { NavController,App } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { EditProfilPage } from '../edit-profil/edit-profil';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';
import { UtilsProvider } from '../../providers/utils/utils';
import {ViewController} from 'ionic-angular';
import { ModalController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  username:string;
  photoUrl:string;

  data = {
    fullname: '', 
    username: '',
    email: '',
    password:'',
    confirm_password:'',
    country:'',
    phone:'',
    isAcceptGCU: 1,
    slogan:''
  };
  constructor(public navCtrl: NavController, 
    private app:App, public storage: Storage,
     private utils: UtilsProvider,private viewCtrl: ViewController,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad contactpage');
  }

  ionViewWillEnter(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.username = user.username;
      this.data.fullname = user.fullname;
      this.data.username = user.username;
      this.data.email = user.email;
      this.data.phone = user.phone;
      this.data.country = user.country;
      this.data.slogan = user.slogan;

      this.storage.get('picture').then((picture)=>{
        if (picture) {
          this.photoUrl = picture;
        } else {
          if(user.photo){   
            this.photoUrl = ConstantVariable.mediaUrl+user.photo;
          }else{
            this.photoUrl = 'assets/imgs/profile.png';
          }
        }   
    });
   });
  }
  logout(){
    // this.navCtrl.setRoot(LoginPage,{});
    
    this.storage.remove('token').then(()=>{
      this.storage.remove('user').then(()=>{
        this.storage.remove('token_type').then(()=>{
          this.app.getRootNav().setRoot(LoginPage);
                })
        .catch(()=>{
          this.utils.makeToast('Veuillez réessayer svp!').present();
        });
      })
      .catch(()=>{
        this.utils.makeToast('Veuillez réessayer svp!').present();
      });
    })
    .catch(()=>{
      this.utils.makeToast('Veuillez réessayer svp!').present();
    });
  }
  edit(){
    this.navCtrl.push('EditProfilPage',{});
  }

  videoList(){
    this.navCtrl.push('VideoReçuesPage',{});
  }

  demandesList(){
    this.navCtrl.push('DemandesListPage',{});
  }

  picture(){
    this.navCtrl.push('PicturePage',{});
  }

  presentModal() {
    const modal = this.modalCtrl.create('PicturePage');
    modal.onDidDismiss(data => {
      console.log(data);
      this.viewCtrl._willEnter();
    });
    modal.present();
  }
}

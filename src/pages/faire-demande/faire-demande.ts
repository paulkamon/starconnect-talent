import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the FaireDemandePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faire-demande',
  templateUrl: 'faire-demande.html',
})
export class FaireDemandePage {
  star:any;
  data = {
    talent_id: '',
    fan_id: '',
    fan_instructions: '',
    request_is_for: 'myself'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,private connectivityService: ConnectivityProvider,
    private userService: UserProvider,
    private utils: UtilsProvider,
    public storage: Storage) {

      this.star=this.navParams.get('star');
      this.data.talent_id=this.star.id;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaireDemandePage');
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.data.fan_id = user.id;
    })
  }

  submit() {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('...');
      if (this.data.fan_instructions) {
      // loader.present().then(()=>{
      //   this.userService.faireDemande(this.data).then(response =>{
      //       loader.dismiss();
      //       // this.response = response;
      //       console.log(response);
            
      //   }).catch((error)=>{
      //     console.log(error);
  
      //     loader.dismiss();
      //     this.utils.makeToast('erreur lors de la demande').present();
      //   })
      // })

      this.navCtrl.push('PaymentPage',{star: this.star , data: this.data})
    }else{
      let toast = this.utils.makeToast('Veuillez entrer les Instructions');
      toast.present();
    }
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}

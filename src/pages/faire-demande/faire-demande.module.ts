import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaireDemandePage } from './faire-demande';

@NgModule({
  declarations: [
    FaireDemandePage,
  ],
  imports: [
    IonicPageModule.forChild(FaireDemandePage),
  ],
})
export class FaireDemandePageModule {}

import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { ConstantVariable } from '../../app/constant-variable';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { UtilsProvider } from '../utils/utils';

declare var cordova: any;
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
token;

  constructor(public http: HTTP , public storage: Storage, private transfer: FileTransfer, private utils: UtilsProvider,) {
    console.log('Hello UserProvider Provider');
    this.storage.get('token').then((token)=>{
      console.log(token);
      this.token = token;
      })
  }

  listTalents(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl+ConstantVariable.talentsList, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  
  listDemandesReçues(token) {
  
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl+ConstantVariable.DemandesReçuesList, {},{ 'Content-Type': "application/json", 'Accept': "application/json", 'Authorization': "Bearer " + token }).then((data)=>{
        let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

  
  walletDetails(token) {
  
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl+ConstantVariable.walletDetails, {},{ 'Content-Type': "application/json", 'Accept': "application/json", 'Authorization': "Bearer " + token }).then((data)=>{
        let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }
  
  faireDemande(body) : Promise<any> {
		console.log(body);
		return new Promise<any>((resolve,reject) => {

      this.http.post(ConstantVariable.apiUrl+ConstantVariable.faireDemande, body, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject(error);
        })
						
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }
  
  postVideo(body) : Promise<any> {
		console.log(body);
		return new Promise<any>((resolve,reject) => {

      this.http.post(ConstantVariable.apiUrl+ConstantVariable.uploadVideo, body, { 'Content-Type': "application/json", 'Accept': "application/json", 'Authorization': "Bearer " + this.token }).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
						
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }
  
   updatepp(body,token) : Promise<any> {
		console.log(body);
		return new Promise<any>((resolve,reject) => {

      this.http.post(ConstantVariable.apiUrl+ConstantVariable.updatepp, body, { 'Content-Type': "application/json", 'Accept': "application/json", 'Authorization': "Bearer " + token }).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
						
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  userInformation(body,token) : Promise<any> {
		return new Promise<any>((resolve,reject) => {

      this.http.post(ConstantVariable.apiUrl+ConstantVariable.searchUserUrl, body, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject(error);
        })
						
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
	}
 

  // Always get the accurate path to your apps folder
	pathForImage(img) {
		if (img === null) {
			return '';
		} else {
			return cordova.file.dataDirectory + img;
		}
	}

	// storeVideo(lastImage, body): Promise<any> {
    storeVideo(pathImage,lastImage, body, token): Promise<any> {

		return new Promise<any>((resolve,reject) => {
				// Destination URL
        let url = encodeURI(ConstantVariable.apiUrl+ConstantVariable.uploadVideo);
        // let url = encodeURI("http://wakodemo.tk:3131/media/uploadFile")
				// File for Upload
        // let targetPath = this.pathForImage(lastImage);
        // let targetPath = this.pathForImage(lastImage);
        let targetPath = pathImage;
        console.log("targetPath "+targetPath);
				// let options = {
				// 	fileKey: "sourcefile",
				// 	fileName: lastImage,
				// 	chunkedMode: false,
				// 	params: body,
				// 	// headers : {'Content-Type': "application/json", 'Accept': "application/json", 'Authorization':'Bearer '+this.token}
        // };
        
        let options : FileUploadOptions = {
					fileKey: "media",
					fileName: lastImage,
					chunkedMode: false,
					params: body,
					headers : {'Accept': "application/json", 'Authorization':'Bearer '+token}
        };
        
				const fileTransfer: FileTransferObject = this.transfer.create();

				// Use the FileTransfer to upload the image
				fileTransfer.upload(targetPath, url, options).then(results => {
          let resp = JSON.parse(results.response);
					// let resp = JSON.parse(results.response).data;
          resolve(resp);
					
				}, err => {
					console.error(err);
					reject();
					// this.utils.makeToast('Erreur lors de l\'actualisation.').present();
				});
            })
            .then((uu)=> uu)
            .catch(err => Promise.reject(err || 'error'));
	}

}

import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';
import { App } from "ionic-angular";
import { NavController } from "ionic-angular/index";

@Injectable()
export class AuthServiceProvider {
	private navCtrl: any;

	constructor(private storage: Storage,
		public http: HTTP,
		private app:App) {
			this.navCtrl = app.getActiveNav();
	}

	//headers.append("Authorization","Bearer "+token);
	login(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.loginUrl, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	register(body) : Promise<any> {

		console.log(body);
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.registerUrl, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			})
			.catch((error)=>{
				reject(error);
			})
						
            })
            .then((uu)=> uu)
            .catch(err => Promise.reject(err || 'err'));
	}

	resetPwd(body) : Promise<any> {

		console.log(body);
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.passwordCreate, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			})
			.catch((error)=>{
				reject(error);
			})
						
            })
            .then((uu)=> uu)
            .catch(err => Promise.reject(err || 'err'));
	}
}
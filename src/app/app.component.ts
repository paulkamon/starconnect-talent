import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
// import { TalentDashboardPage } from '../pages/talent-dashboard/talent-dashboard';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';

// import { TranslateService } from '@ngx-translate/core';

import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  data:any;

  constructor(platform: Platform, statusBar: StatusBar,
     splashScreen: SplashScreen, public storage: Storage,private oneSignal: OneSignal /* translate: TranslateService */) {
    // translate.setDefaultLang('fr');
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      
      this.oneSignal.startInit('eea0c177-1709-4dcb-871c-930136e7f0ae', '371218059094');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
  
    //   // for debugging purposes
    // this.oneSignal.setLogLevel({ logLevel: 4, visualLevel: 4 });

      this.oneSignal.handleNotificationReceived().subscribe((data: any) => {
      // do something when notification is received
      console.log('push notification received, data: ', data);
      });
  
      this.oneSignal.handleNotificationOpened().subscribe((data: any) => {
        // do something when a notification is opened
        console.log('push notification opened, data: ', data);
        // this.nav.setRoot(MyordersPage);
      });
      // window['plugins'].OneSignal.setExternalUserId('my_unique_name');
      this.oneSignal.endInit();
      
      this.storage.get('token').then((data)=>{
        this.data = data;
        if (this.data) {
          this.rootPage = 'TalentDashboardPage';
        }else{
          this.rootPage=LoginPage;
        }
      })

    });
  }
}

import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Network } from '@ionic-native/network';

// MODULES
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';

import { HttpClientModule, HttpClientJsonpModule, HttpClient  } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MbscModule } from '@mobiscroll/angular';

import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { MediaCapture} from '@ionic-native/media-capture';

import { UtilsProvider } from '../providers/utils/utils';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { UserProvider } from '../providers/user/user';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';

// import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
// import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { Http } from '@angular/http';
import { LoginPageModule } from '../pages/login/login.module';

import { AndroidPermissions } from '@ionic-native/android-permissions';

// export function createTranslateLoader(http: Http) {
//  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
// }


import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

import { OneSignal } from '@ionic-native/onesignal';

import { VideoCapturePlus} from '@ionic-native/video-capture-plus';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
  ],
  imports: [ 
    FormsModule, 
    MbscModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserModule,
    HttpModule,
    LoginPageModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    // TranslateModule.forRoot({
    //   loader: {
    //    provide: TranslateLoader,
    //    useFactory: (createTranslateLoader),
    //    deps: [Http]
    //  }
    // })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' },
    StatusBar,
    SplashScreen,
    Network,
    HTTP,
    Camera,
    File,
    AndroidPermissions,
    FileTransfer,
    FileTransferObject,
    MediaCapture,
    VideoCapturePlus,
    UtilsProvider,
    AuthServiceProvider,
    UserProvider,
    ConnectivityProvider,
    OneSignal,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
